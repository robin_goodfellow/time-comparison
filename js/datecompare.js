/* JavaScript to;
 * 
 * - load Timezones into select-menus
 * - set default timezone to current locale
 * - initialise the dateTimePickers
 * - apply selected timezones to input date-times
 * - post date-times to PHP script
 * - insert results into div on page
 * 
 * - author: Robin Goodfellow
 */

function fetchCurrentTimezone () {
  // fetch current tz formatted to match JavaScript requirements
  var d = new Date().toString();
  var zone = d.substring(d.indexOf('GMT'),d.length);
  return zone;
}

function applySelectedTimezone (dateValue, selectedZone) {
  // return substring of stringified dateValue with timezone replaced
  return dateValue.toString().substring(0, dateValue.toString().indexOf('GMT')) + selectedZone;
}

function postData () {
  if ($('#dateTimeA').datetimepicker('getValue') != null && $('#dateTimeB').datetimepicker('getValue') != null) {
    // apply selected timezones
    var dateA = applySelectedTimezone($('#dateTimeA').datetimepicker('getValue'), $('#timeZoneA').val());
    var dateB = applySelectedTimezone($('#dateTimeB').datetimepicker('getValue'), $('#timeZoneB').val());
    var converter = $('#converter option:selected').val();
    // post to PHP script
    var postDates = $.post({
      url: 'inc/datecompare.php',
      data: { dateA:dateA, dateB:dateB, converter:converter }
    }).done(function(data) {
      displayResults(data);
    });
  }
}

function displayResults (data) {
  var dataObj = JSON.parse(data);
  // format and display the results in the #results div
  var bgStatus;
  if (dataObj.status == 'success') {
    bgStatus = 'success';
  } else {
    bgStatus = 'danger';
  }
  var htmlString = '<ul class="list-group">';
  for (key in dataObj.message) {
    if (!!dataObj.message[key]) {
      htmlString += '<li class="list-group-item list-group-item-' + bgStatus + '">' + key.replace('_',' ') + ((key.indexOf('converted') > -1) ? ' to ' + $('#converter option:selected').val() : '') + ': ' + dataObj.message[key] + '</li>';
    }
  }
  htmlString += '</ul>'
  $('#results').html(htmlString);
}

$(document).ready(function() {
  // fill the timezone select menus
  for (zone in timezones) {
    $('#timeZoneA, #timeZoneB').append('<option value="' + timezones[zone].offset + ' (' + timezones[zone].abbr + ')">' + timezones[zone].offset + ' ' + timezones[zone].abbr + ' ' + timezones[zone].name + '</option>');
  }
  // set default timezones to current
  var tz = fetchCurrentTimezone();
  $('#timeZoneA, #timeZoneB').val(tz);
  // functions of the datetimepickers
  $('#dateTimeA, #dateTimeB').datetimepicker({
    lang: 'en',
    onChangeDateTime: function() {
      postData();
    }
  });
  // if a converter is chosen
  $('#converter, #timeZoneA, #timeZoneB').change(function() {
    postData();
  });
});
