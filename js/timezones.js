/*
 * array of global timezones
 * modified to match the jquery.datetimepicker usage of GMT instead of UTC
 * Source: https://en.wikipedia.org/wiki/List_of_time_zone_abbreviations
 */

var timezones = [
  {
    abbr:"BIT",
    name:"Baker Island Time",
    offset:"GMT-12"
  },
  {
    abbr:"NUT",
    name:"Niue Time",
    offset:"GMT-11"
  },
  {
    abbr:"SST",
    name:"Samoa Standard Time",
    offset:"GMT-11"
  },
  {
    abbr:"CKT",
    name:"Cook Island Time",
    offset:"GMT-10"
  },
  {
    abbr:"HAST",
    name:"Hawaii-Aleutian Standard Time",
    offset:"GMT-10"
  },
  {
    abbr:"TAHT",
    name:"Tahiti Time",
    offset:"GMT-10"
  },
  {
    abbr:"MART",
    name:"Marquesas Islands Time",
    offset:"GMT-0930"
  },
  {
    abbr:"MIT",
    name:"Marquesas Islands Time",
    offset:"GMT-0930"
  },
  {
    abbr:"AKST",
    name:"Alaska Standard Time",
    offset:"GMT-09"
  },
  {
    abbr:"GAMT",
    name:"Gambier Islands",
    offset:"GMT-09"
  },
  {
    abbr:"GIT",
    name:"Gambier Island Time",
    offset:"GMT-09"
  },
  {
    abbr:"HADT",
    name:"Hawaii-Aleutian Daylight Time",
    offset:"GMT-09"
  },
  {
    abbr:"AKDT",
    name:"Alaska Daylight Time",
    offset:"GMT-08"
  },
  {
    abbr:"CIST",
    name:"Clipperton Island Standard Time",
    offset:"GMT-08"
  },
  {
    abbr:"PST",
    name:"Pacific Standard Time (North America)",
    offset:"GMT-08"
  },
  {
    abbr:"MST",
    name:"Mountain Standard Time (North America)",
    offset:"GMT-07"
  },
  {
    abbr:"PDT",
    name:"Pacific Daylight Time (North America)",
    offset:"GMT-07"
  },
  {
    abbr:"CST",
    name:"Central Standard Time (North America)",
    offset:"GMT-06"
  },
  {
    abbr:"EAST",
    name:"Easter Island Standard Time",
    offset:"GMT-06"
  },
  {
    abbr:"GALT",
    name:"Galapagos Time",
    offset:"GMT-06"
  },
  {
    abbr:"MDT",
    name:"Mountain Daylight Time (North America)",
    offset:"GMT-06"
  },
  {
    abbr:"ACT",
    name:"Acre Time",
    offset:"GMT-05"
  },
  {
    abbr:"CDT",
    name:"Central Daylight Time (North America)",
    offset:"GMT-05"
  },
  {
    abbr:"COT",
    name:"Colombia Time",
    offset:"GMT-05"
  },
  {
    abbr:"CST",
    name:"Cuba Standard Time",
    offset:"GMT-05"
  },
  {
    abbr:"EASST",
    name:"Easter Island Summer Time",
    offset:"GMT-05"
  },
  {
    abbr:"ECT",
    name:"Ecuador Time",
    offset:"GMT-05"
  },
  {
    abbr:"EST",
    name:"Eastern Standard Time (North America)",
    offset:"GMT-05"
  },
  {
    abbr:"PET",
    name:"Peru Time",
    offset:"GMT-05"
  },
  {
    abbr:"AMT",
    name:"Amazon Time (Brazil)",
    offset:"GMT-04"
  },
  {
    abbr:"AST",
    name:"Atlantic Standard Time",
    offset:"GMT-04"
  },
  {
    abbr:"BOT",
    name:"Bolivia Time",
    offset:"GMT-04"
  },
  {
    abbr:"CDT",
    name:"Cuba Daylight Time",
    offset:"GMT-04"
  },
  {
    abbr:"CLT",
    name:"Chile Standard Time",
    offset:"GMT-04"
  },
  {
    abbr:"COST",
    name:"Colombia Summer Time",
    offset:"GMT-04"
  },
  {
    abbr:"ECT",
    name:"Eastern Caribbean Time (does not recognise DST)",
    offset:"GMT-04"
  },
  {
    abbr:"EDT",
    name:"Eastern Daylight Time (North America)",
    offset:"GMT-04"
  },
  {
    abbr:"FKT",
    name:"Falkland Islands Time",
    offset:"GMT-04"
  },
  {
    abbr:"GYT",
    name:"Guyana Time",
    offset:"GMT-04"
  },
  {
    abbr:"PYT",
    name:"Paraguay Time (South America)",
    offset:"GMT-04"
  },
  {
    abbr:"VET",
    name:"Venezuelan Standard Time",
    offset:"GMT-04"
  },
  {
    abbr:"NST",
    name:"Newfoundland Standard Time",
    offset:"GMT-0330"
  },
  {
    abbr:"NT",
    name:"Newfoundland Time",
    offset:"GMT-0330"
  },
  {
    abbr:"ADT",
    name:"Atlantic Daylight Time",
    offset:"GMT-03"
  },
  {
    abbr:"AMST",
    name:"Amazon Summer Time (Brazil)",
    offset:"GMT-03"
  },
  {
    abbr:"ART",
    name:"Argentina Time",
    offset:"GMT-03"
  },
  {
    abbr:"BRT",
    name:"Brasilia Time",
    offset:"GMT-03"
  },
  {
    abbr:"CLST",
    name:"Chile Summer Time",
    offset:"GMT-03"
  },
  {
    abbr:"FKST",
    name:"Falkland Islands Summer Time",
    offset:"GMT-03"
  },
  {
    abbr:"GFT",
    name:"French Guiana Time",
    offset:"GMT-03"
  },
  {
    abbr:"PMST",
    name:"Saint Pierre and Miquelon Standard Time",
    offset:"GMT-03"
  },
  {
    abbr:"PYST",
    name:"Paraguay Summer Time (South America)",
    offset:"GMT-03"
  },
  {
    abbr:"ROTT",
    name:"Rothera Research Station Time",
    offset:"GMT-03"
  },
  {
    abbr:"SRT",
    name:"Suriname Time",
    offset:"GMT-03"
  },
  {
    abbr:"UYT",
    name:"Uruguay Standard Time",
    offset:"GMT-03"
  },
  {
    abbr:"NDT",
    name:"Newfoundland Daylight Time",
    offset:"GMT-0230"
  },
  {
    abbr:"BRST",
    name:"Brasilia Summer Time",
    offset:"GMT-02"
  },
  {
    abbr:"FNT",
    name:"Fernando de Noronha Time",
    offset:"GMT-02"
  },
  {
    abbr:"GST",
    name:"South Georgia and the South Sandwich Islands",
    offset:"GMT-02"
  },
  {
    abbr:"PMDT",
    name:"Saint Pierre and Miquelon Daylight time",
    offset:"GMT-02"
  },
  {
    abbr:"UYST",
    name:"Uruguay Summer Time",
    offset:"GMT-02"
  },
  {
    abbr:"AZOT",
    name:"Azores Standard Time",
    offset:"GMT-01"
  },
  {
    abbr:"CVT",
    name:"Cape Verde Time",
    offset:"GMT-01"
  },
  {
    abbr:"EGT",
    name:"Eastern Greenland Time",
    offset:"GMT-01"
  },
  {
    abbr:"AZOST",
    name:"Azores Summer Time",
    offset:"GMT±00"
  },
  {
    abbr:"EGST",
    name:"Eastern Greenland Summer Time",
    offset:"GMT±00"
  },
  {
    abbr:"GMT",
    name:"Greenwich Mean Time",
    offset:"GMT±00"
  },
  {
    abbr:"UTC",
    name:"Coordinated Universal Time",
    offset:"GMT±00"
  },
  {
    abbr:"WET",
    name:"Western European Time",
    offset:"GMT±00"
  },
  {
    abbr:"BST",
    name:"British Summer Time (British Standard Time from Feb 1968 to Oct 1971)",
    offset:"GMT+01"
  },
  {
    abbr:"CET",
    name:"Central European Time",
    offset:"GMT+01"
  },
  {
    abbr:"DFT",
    name:"AIX specific equivalent of Central European Time",
    offset:"GMT+01"
  },
  {
    abbr:"IST",
    name:"Irish Standard Time",
    offset:"GMT+01"
  },
  {
    abbr:"MET",
    name:"Middle European Time Same zone as CET",
    offset:"GMT+01"
  },
  {
    abbr:"WAT",
    name:"West Africa Time",
    offset:"GMT+01"
  },
  {
    abbr:"WEST",
    name:"Western European Summer Time",
    offset:"GMT+01"
  },
  {
    abbr:"CAT",
    name:"Central Africa Time",
    offset:"GMT+02"
  },
  {
    abbr:"CEST",
    name:"Central European Summer Time (Cf. HAEC)",
    offset:"GMT+02"
  },
  {
    abbr:"EET",
    name:"Eastern European Time",
    offset:"GMT+02"
  },
  {
    abbr:"HAEC",
    name:"Heure Avancée d'Europe Centrale francised name for CEST",
    offset:"GMT+02"
  },
  {
    abbr:"IST",
    name:"Israel Standard Time",
    offset:"GMT+02"
  },
  {
    abbr:"MEST",
    name:"Middle European Summer Time Same zone as CEST",
    offset:"GMT+02"
  },
  {
    abbr:"SAST",
    name:"South African Standard Time",
    offset:"GMT+02"
  },
  {
    abbr:"USZ1",
    name:"Kaliningrad Time",
    offset:"GMT+02"
  },
  {
    abbr:"WAST",
    name:"West Africa Summer Time",
    offset:"GMT+02"
  },
  {
    abbr:"AST",
    name:"Arabia Standard Time",
    offset:"GMT+03"
  },
  {
    abbr:"EAT",
    name:"East Africa Time",
    offset:"GMT+03"
  },
  {
    abbr:"EEST",
    name:"Eastern European Summer Time",
    offset:"GMT+03"
  },
  {
    abbr:"FET",
    name:"Further-eastern European Time",
    offset:"GMT+03"
  },
  {
    abbr:"IDT",
    name:"Israel Daylight Time",
    offset:"GMT+03"
  },
  {
    abbr:"IOT",
    name:"Indian Ocean Time",
    offset:"GMT+03"
  },
  {
    abbr:"MSK",
    name:"Moscow Time",
    offset:"GMT+03"
  },
  {
    abbr:"SYOT",
    name:"Showa Station Time",
    offset:"GMT+03"
  },
  {
    abbr:"TRT",
    name:"Turkey Time",
    offset:"GMT+03"
  },
  {
    abbr:"IRST",
    name:"Iran Standard Time",
    offset:"GMT+0330"
  },
  {
    abbr:"AMT",
    name:"Armenia Time",
    offset:"GMT+04"
  },
  {
    abbr:"AZT",
    name:"Azerbaijan Time",
    offset:"GMT+04"
  },
  {
    abbr:"GET",
    name:"Georgia Standard Time",
    offset:"GMT+04"
  },
  {
    abbr:"GST",
    name:"Gulf Standard Time",
    offset:"GMT+04"
  },
  {
    abbr:"MUT",
    name:"Mauritius Time",
    offset:"GMT+04"
  },
  {
    abbr:"RET",
    name:"Réunion Time",
    offset:"GMT+04"
  },
  {
    abbr:"SAMT",
    name:"Samara Time",
    offset:"GMT+04"
  },
  {
    abbr:"SCT",
    name:"Seychelles Time",
    offset:"GMT+04"
  },
  {
    abbr:"VOLT",
    name:"Volgograd Time",
    offset:"GMT+04"
  },
  {
    abbr:"AFT",
    name:"Afghanistan Time",
    offset:"GMT+0430"
  },
  {
    abbr:"IRDT",
    name:"Iran Daylight Time",
    offset:"GMT+0430"
  },
  {
    abbr:"HMT",
    name:"Heard and McDonald Islands Time",
    offset:"GMT+05"
  },
  {
    abbr:"MAWT",
    name:"Mawson Station Time",
    offset:"GMT+05"
  },
  {
    abbr:"MVT",
    name:"Maldives Time",
    offset:"GMT+05"
  },
  {
    abbr:"ORAT",
    name:"Oral Time",
    offset:"GMT+05"
  },
  {
    abbr:"PKT",
    name:"Pakistan Standard Time",
    offset:"GMT+05"
  },
  {
    abbr:"TFT",
    name:"Indian/Kerguelen",
    offset:"GMT+05"
  },
  {
    abbr:"TJT",
    name:"Tajikistan Time",
    offset:"GMT+05"
  },
  {
    abbr:"TMT",
    name:"Turkmenistan Time",
    offset:"GMT+05"
  },
  {
    abbr:"UZT",
    name:"Uzbekistan Time",
    offset:"GMT+05"
  },
  {
    abbr:"YEKT",
    name:"Yekaterinburg Time",
    offset:"GMT+05"
  },
  {
    abbr:"IST",
    name:"Indian Standard Time",
    offset:"GMT+0530"
  },
  {
    abbr:"SLST",
    name:"Sri Lanka Standard Time",
    offset:"GMT+0530"
  },
  {
    abbr:"NPT",
    name:"Nepal Time",
    offset:"GMT+0545"
  },
  {
    abbr:"BIOT",
    name:"British Indian Ocean Time",
    offset:"GMT+06"
  },
  {
    abbr:"BST",
    name:"Bangladesh Standard Time",
    offset:"GMT+06"
  },
  {
    abbr:"BTT",
    name:"Bhutan Time",
    offset:"GMT+06"
  },
  {
    abbr:"KGT",
    name:"Kyrgyzstan time",
    offset:"GMT+06"
  },
  {
    abbr:"OMST",
    name:"Omsk Time",
    offset:"GMT+06"
  },
  {
    abbr:"VOST",
    name:"Vostok Station Time",
    offset:"GMT+06"
  },
  {
    abbr:"CCT",
    name:"Cocos Islands Time",
    offset:"GMT+0630"
  },
  {
    abbr:"MMT",
    name:"Myanmar Standard Time",
    offset:"GMT+0630"
  },
  {
    abbr:"ACT",
    name:"ASEAN Common Time",
    offset:"GMT+0630 - UTC+09"
  },
  {
    abbr:"CXT",
    name:"Christmas Island Time",
    offset:"GMT+07"
  },
  {
    abbr:"DAVT",
    name:"Davis Time",
    offset:"GMT+07"
  },
  {
    abbr:"HOVT",
    name:"Khovd Standard Time",
    offset:"GMT+07"
  },
  {
    abbr:"ICT",
    name:"Indochina Time",
    offset:"GMT+07"
  },
  {
    abbr:"KRAT",
    name:"Krasnoyarsk Time",
    offset:"GMT+07"
  },
  {
    abbr:"THA",
    name:"Thailand Standard Time",
    offset:"GMT+07"
  },
  {
    abbr:"WIT",
    name:"Western Indonesian Time",
    offset:"GMT+07"
  },
  {
    abbr:"AWST",
    name:"Australian Western Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"BDT",
    name:"Brunei Time",
    offset:"GMT+08"
  },
  {
    abbr:"CHOT",
    name:"Choibalsan Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"CIT",
    name:"Central Indonesia Time",
    offset:"GMT+08"
  },
  {
    abbr:"CST",
    name:"China Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"CT",
    name:"China time",
    offset:"GMT+08"
  },
  {
    abbr:"HKT",
    name:"Hong Kong Time",
    offset:"GMT+08"
  },
  {
    abbr:"HOVST",
    name:"Khovd Summer Time",
    offset:"GMT+08"
  },
  {
    abbr:"IRKT",
    name:"Irkutsk Time",
    offset:"GMT+08"
  },
  {
    abbr:"MST",
    name:"Malaysia Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"MYT",
    name:"Malaysia Time",
    offset:"GMT+08"
  },
  {
    abbr:"PHT",
    name:"Philippine Time",
    offset:"GMT+08"
  },
  {
    abbr:"PST",
    name:"Philippine Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"SGT",
    name:"Singapore Time",
    offset:"GMT+08"
  },
  {
    abbr:"SST",
    name:"Singapore Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"ULAT",
    name:"Ulaanbaatar Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"WST",
    name:"Western Standard Time",
    offset:"GMT+08"
  },
  {
    abbr:"CWST",
    name:"Central Western Standard Time (Australia) unofficial",
    offset:"GMT+0845"
  },
  {
    abbr:"CHOST",
    name:"Choibalsan Summer Time",
    offset:"GMT+09"
  },
  {
    abbr:"EIT",
    name:"Eastern Indonesian Time",
    offset:"GMT+09"
  },
  {
    abbr:"JST",
    name:"Japan Standard Time",
    offset:"GMT+09"
  },
  {
    abbr:"KST",
    name:"Korea Standard Time",
    offset:"GMT+09"
  },
  {
    abbr:"TLT",
    name:"Timor Leste Time",
    offset:"GMT+09"
  },
  {
    abbr:"ULAST",
    name:"Ulaanbaatar Summer Time",
    offset:"GMT+09"
  },
  {
    abbr:"YAKT",
    name:"Yakutsk Time",
    offset:"GMT+09"
  },
  {
    abbr:"ACST",
    name:"Australian Central Standard Time",
    offset:"GMT+0930"
  },
  {
    abbr:"ACST",
    name:"Central Standard Time (Australia)",
    offset:"GMT+0930"
  },
  {
    abbr:"AEST",
    name:"Australian Eastern Standard Time",
    offset:"GMT+10"
  },
  {
    abbr:"CHST",
    name:"Chamorro Standard Time",
    offset:"GMT+10"
  },
  {
    abbr:"CHUT",
    name:"Chuuk Time",
    offset:"GMT+10"
  },
  {
    abbr:"DDUT",
    name:"Dumont d'Urville Time",
    offset:"GMT+10"
  },
  {
    abbr:"AEST",
    name:"Eastern Standard Time (Australia)",
    offset:"GMT+10"
  },
  {
    abbr:"PGT",
    name:"Papua New Guinea Time",
    offset:"GMT+10"
  },
  {
    abbr:"VLAT",
    name:"Vladivostok Time",
    offset:"GMT+10"
  },
  {
    abbr:"ACDT",
    name:"Australian Central Daylight Savings Time",
    offset:"GMT+1030"
  },
  {
    abbr:"ACDT",
    name:"Central Summer Time (Australia)",
    offset:"GMT+1030"
  },
  {
    abbr:"LHST",
    name:"Lord Howe Standard Time",
    offset:"GMT+1030"
  },
  {
    abbr:"AEDT",
    name:"Australian Eastern Daylight Savings Time",
    offset:"GMT+11"
  },
  {
    abbr:"BST",
    name:"Bougainville Standard Time",
    offset:"GMT+11"
  },
  {
    abbr:"AEDT",
    name:"Eastern Summer Time (Australia)",
    offset:"GMT+11"
  },
  {
    abbr:"KOST",
    name:"Kosrae Time",
    offset:"GMT+11"
  },
  {
    abbr:"LHST",
    name:"Lord Howe Summer Time",
    offset:"GMT+11"
  },
  {
    abbr:"MIST",
    name:"Macquarie Island Station Time",
    offset:"GMT+11"
  },
  {
    abbr:"NCT",
    name:"New Caledonia Time",
    offset:"GMT+11"
  },
  {
    abbr:"NFT",
    name:"Norfolk Time",
    offset:"GMT+11"
  },
  {
    abbr:"PONT",
    name:"Pohnpei Standard Time",
    offset:"GMT+11"
  },
  {
    abbr:"SAKT",
    name:"Sakhalin Island time",
    offset:"GMT+11"
  },
  {
    abbr:"SBT",
    name:"Solomon Islands Time",
    offset:"GMT+11"
  },
  {
    abbr:"SRET",
    name:"Srednekolymsk Time",
    offset:"GMT+11"
  },
  {
    abbr:"VUT",
    name:"Vanuatu Time",
    offset:"GMT+11"
  },
  {
    abbr:"FJT",
    name:"Fiji Time",
    offset:"GMT+12"
  },
  {
    abbr:"GILT",
    name:"Gilbert Island Time",
    offset:"GMT+12"
  },
  {
    abbr:"MAGT",
    name:"Magadan Time",
    offset:"GMT+12"
  },
  {
    abbr:"MHT",
    name:"Marshall Islands",
    offset:"GMT+12"
  },
  {
    abbr:"NZST",
    name:"New Zealand Standard Time",
    offset:"GMT+12"
  },
  {
    abbr:"PETT",
    name:"Kamchatka Time",
    offset:"GMT+12"
  },
  {
    abbr:"TVT",
    name:"Tuvalu Time",
    offset:"GMT+12"
  },
  {
    abbr:"WAKT",
    name:"Wake Island Time",
    offset:"GMT+12"
  },
  {
    abbr:"CHAST",
    name:"Chatham Standard Time",
    offset:"GMT+1245"
  },
  {
    abbr:"NZDT",
    name:"New Zealand Daylight Time",
    offset:"GMT+13"
  },
  {
    abbr:"PHOT",
    name:"Phoenix Island Time",
    offset:"GMT+13"
  },
  {
    abbr:"TKT",
    name:"Tokelau Time",
    offset:"GMT+13"
  },
  {
    abbr:"TOT",
    name:"Tonga Time",
    offset:"GMT+13"
  },
  {
    abbr:"CHADT",
    name:"Chatham Daylight Time",
    offset:"GMT+1345"
  },
  {
    abbr:"LINT",
    name:"Line Islands Time",
    offset:"GMT+14"
  }
];