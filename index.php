<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Date Comparison</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.datetimepicker.css">
  </head>
  <body>
    <div class="container-fluid">
      <h1>Date-Time Compare</h1>
      <form class="form-horizontal">
        <div class="col-xs-6 col-sm-5 col-md-4 col-lg-3 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
          <div class="form-group">
            <label>compare this date</label>
            <input type="text" id="dateTimeA" class="form-control">
          </div>
        </div>
        <div class="col-xs-5 col-lg-5">
          <div class="form-group">
            <label>time zone</label>
            <select id="timeZoneA" class="form-control"></select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-5 col-md-4 col-lg-3 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
          <div class="form-group">
            <label>with this date</label>
            <input type="text" id="dateTimeB" class="form-control">
          </div>
        </div>
        <div class="col-xs-5 col-lg-5">
          <div class="form-group">
            <label>time zone</label>
            <select id="timeZoneB" class="form-control"></select>
          </div>
        </div>
        <div class="col-xs-6 col-sm-5 col-md-4 col-lg-3 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
          <div class="form-group">
            <label>convert to</label>
            <select id="converter" class="form-control">
              <option value="">don't convert</option>
              <option value="seconds">seconds</option>
              <option value="minutes">minutes</option>
              <option value="hours">hours</option>
              <option value="years">years</option>
            </select>
          </div>
        </div>
      </form>
      <div class="col-xs-11 col-sm-10 col-md-9 col-lg-8 col-xs-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
        <div id="results"></div>
    </div>
    </div>
    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.datetimepicker.full.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/timezones.js"></script>
    <script type="text/javascript" src="js/datecompare.js"></script>
  </body>
</html>