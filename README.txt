The tasks outlined below appeared clear at first;

1. Find out the number of days between two datetime parameters.
2. Find out the number of weekdays between two datetime parameters.
3. Find out the number of complete weeks between two datetime parameters.
4. Accept a third parameter to convert the result of (1, 2 or 3) into one of seconds, minutes, hours, years.
5. Allow the specification of a timezone for comparison of input parameters from different timezones. 

The difference between two date-time objects is easily calculated using a single PHP function. The number of days can be retrieved from this difference using a format. From this can be calculated the number of complete weeks (7-day periods) and the number of weekdays, using an algorithm. These values can then be readily converted to seconds, minutes, hours, and years.

However, the difference format only collects an integer value of complete days, and is not going to be useful when calculating the number of seconds, minutes, and hours, especially where differing timezones are going to be used as part of the date-time objects.

The original assumption had been to use the difference as an integer. It was only when running final tests that the need became clear to take into account partial days - counting days from time-a to time-b in a similar way to complete-weeks which were being calculated from day-a to day-b.

Modifications were made to correct the previous assumptions thereby giving more significance to the time of day and the timezones for the calculations of the number of days and number of weekdays.

The difference between two days takes into account the times of day for both days and their timezones.
The number of weekdays between the two datatime objects takes into account the times of day for both days and their timezones.
The number of complete weeks between two datetime objects takes into account the times of day for both days and their timezones.
These values are then more accurately able to be converted into seconds, minutes, hours, or years.
