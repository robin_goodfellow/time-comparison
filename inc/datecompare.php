<?php
/* PHP script to;
 * 
 * 1. Find out the number of days between two datetime parameters.
 * 2. Find out the number of weekdays between two datetime parameters.
 * 3. Find out the number of complete weeks between two datetime parameters.
 * 4. Accept a third parameter to convert the result of (1, 2 or 3) into one of seconds, minutes, hours, years.
 * 5. Allow the specification of a timezone for comparison of input parameters from different timezones.
 * 
 * Procedure;
 * 1. receive $_POST[data];
 * 2. compare dates
 * 3. prepare output
 * 4. return data
 * 
 * author: Robin Goodfellow
*/

$dateProcessor = new DateProcessor;
print(json_encode($dateProcessor->processDates()));

class DateProcessor {
	private $dateA;
	private $dateB;
	private $converter;
	private $response;
	
	function __construct() {
		$this->dateA = new DateTime($_POST['dateA']);
		$this->dateB = new DateTime($_POST['dateB']);
		$this->converter = $_POST['converter'];
		$this->response = new Response();
	}
	
	public function processDates() {
		// validation of dates
		if ($this->dateA === false) {
			$this->response->setStatus('error');
			$this->response->setMessage('error','Date-From is not a valid date.');
			return $this->returnResults();
		}
		if ($this->dateB === false) {
			$this->response->setStatus('error');
			$this->response->setMessage('error','Date-To is not a valid date.');
			return $this->returnResults();
		}
		// the interval
		$interval = $this->dateA->diff($this->dateB);
		// format the interval
		$days = $this->calculateDays($interval);
		$weekdays = $this->calculateWeekdays($interval);
		$complete_weeks = floor($interval->format('%a')/7);
		// prepare the response message
		$this->response->setStatus('success');
		$this->response->setMessage('days',$days);
		$this->response->setMessage('weekdays',$weekdays);
		$this->response->setMessage('complete_weeks',$complete_weeks);
		// conversions
		if (!is_null($this->converter)) {
			$days_converted = $this->convertResults($days, 1);
			$weekdays_converted = $this->convertResults($weekdays, 1);
			$weeks_converted = $this->convertResults($complete_weeks, 7);
			// prepare conversion messages
			if (!is_null($days_converted)) {
				$this->response->setMessage('days_converted',$days_converted);
			}
			if (!is_null($weekdays_converted)) {
				$this->response->setMessage('weekdays_converted',$weekdays_converted);
			}
			if (!is_null($weeks_converted)) {
				$this->response->setMessage('weeks_converted',$weeks_converted);
			}
		}
		// return the results
		return $this->returnResults();
	}
	
	private function calculateDays($interval) {
		$days = $interval->format('%a');
		$days += ($interval->format('%h')/24);
		$days += ($interval->format('%i')/(24*60));
		$days += ($interval->format('%s')/(24*60*60));
		return $days;
	}
	
	private function calculateWeekdays($interval) {
		if (($this->dateA == $this->dateB) || ($interval->format('%a') < 1)) {
			// same day
			return 0;
		} elseif ($this->dateA < $this->dateB) {
			// sort which day is first
			$startDate = $this->dateA;
			$endDate = $this->dateB;
		} else {
			// sort which day is first
			$startDate = $this->dateB;
			$endDate = $this->dateA;
		}
		// get integer value for start and end days (0-6)
		$startDay = $startDate->format('w');
		$endDay = $endDate->format('w');
		// calculate weekdays
		$weekdays = floor(((($interval->format('%a')) * 5) - (($startDay - $endDay) * 2)) / 7) + 1;
		// subtract 1 day if starting on Sunday
		if ($startDay == 0) {
			$weekdays--;
		}
		// subtract 1 day if ending on Saturday
		if ($endDay == 6) {
			$weekdays--;
		}
		// add 1 day if ending on weekends and beginning time > end time (incomplete day)
		if ((($endDay == 6) || ($endDay == 0)) &&
			($this->timeAsSeconds($startDate) > $this->timeAsSeconds($endDate))
			) {
			$weekdays++;
		}
		return $weekdays;
	}
	
	private function timeAsSeconds($dateObj) {
		$secs = $dateObj->format('s');
		$secs += ($dateObj->format('i') * 60);
		$secs += ($dateObj->format('H') * 60 * 24);
		$secs += $this->timeZoneAdjustment($dateObj->format('P')); // e, O, P and T
		return $secs;
	}
	
	private function timeZoneAdjustment($tz) {
		$sign = substr($tz, 0, 1);
		$hour = intval(substr($tz, 1, 2)) * 60 * 60;
		$mins = intval(substr($tz, 4, 2)) * 60;
		return (($hour + $mins) * ($sign == '-' ? -1 : 1));
	}
	
	private function convertResults($val, $valtype) {
		switch($this->converter) {
			case 'seconds':
				return $val * $valtype * 24 * 60 * 60;
			case 'minutes':
				return $val * $valtype * 24 * 60;
			case 'hours':
				return $val * $valtype * 24;
			case 'years':
				return $val * $valtype / 365.25; // ISO 80000-3
			default:
				return null;
		}
	}
	
	private function returnResults() {
		return [
			'status' => $this->response->getStatus(),
			'message' => $this->response->getMessage()
		];
	}
}

class Response {
	private $status;
	private $message;

	function __construct() {
		$this->status = null;
		$this->message = new Message();
	}
	
	// setters
	public function setStatus($val) {
		$this->status = $val;
	}
	public function setError($val) {
		$this->error = $val;
	}
	public function setMessage($key, $val) {
		switch($key) {
			case 'error':
				$this->message->setError($val);
				break;
			case 'days':
				$this->message->setDays($val);
				break;
			case 'weekdays':
				$this->message->setWeekdays($val);
				break;
			case 'complete_weeks':
				$this->message->setCompleteWeeks($val);
				break;
			case 'conversion_type':
				$this->message->setConversionType($val);
				break;
			case 'days_converted':
				$this->message->setDaysConverted($val);
				break;
			case 'weekdays_converted':
				$this->message->setWeekdaysConverted($val);
				break;
			case 'weeks_converted':
				$this->message->setWeeksConverted($val);
				break;
			default:
				break;
		}
	}
	
	// getters
	public function getStatus() {
		return $this->status;
	}
	public function getMessage() {
		return [
			'error' => $this->message->getError(),
			'days' => $this->message->getDays(),
			'weekdays' => $this->message->getWeekdays(),
			'complete_weeks' => $this->message->getCompleteWeeks(),
			'conversion_type' => $this->message->getConversionType(),
			'days_converted' => $this->message->getDaysConverted(),
			'weekdays_converted' => $this->message->getWeekdaysConverted(),
			'weeks_converted' => $this->message->getWeeksConverted()
		];
	}
}

class Message {
	private $error;
	private $days;
	private $weekdays;
	private $complete_weeks;
	private $conversion_type;
	private $days_converted;
	private $weekdays_converted;
	private $weeks_converted;
	
	function __construct() {
		$this->error = null;
		$this->days = null;
		$this->weekdays = null;
		$this->complete_weeks = null;
		$this->conversion_type = null;
		$this->days_converted = null;
		$this->weekdays_converted = null;
		$this->weeks_converted = null;
	}
	
	// setters
	public function setError($val) {
		$this->error = $val;
	}
	public function setDays($val) {
		$this->days = $val;
	}
	public function setWeekdays($val) {
		$this->weekdays = $val;
	}
	public function setCompleteWeeks($val) {
		$this->complete_weeks = $val;
	}
	public function setConversionType($val) {
		$this->conversion_type = $val;
	}
	public function setDaysConverted($val) {
		$this->days_converted = $val;
	}
	public function setWeekdaysConverted($val) {
		$this->weekdays_converted = $val;
	}
	public function setWeeksConverted($val) {
		$this->weeks_converted = $val;
	}
	
	// getters
	public function getError() {
		return $this->error;
	}
	public function getDays() {
		return $this->days;
	}
	public function getWeekdays() {
		return$this->weekdays;
	}
	public function getCompleteWeeks() {
		return$this->complete_weeks;
	}
	public function getConversionType() {
		return$this->conversion_type;
	}
	public function getDaysConverted() {
		return$this->days_converted;
	}
	public function getWeekdaysConverted() {
		return$this->weekdays_converted;
	}
	public function getWeeksConverted() {
		return $this->weeks_converted;
	}
}
